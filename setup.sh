#!/usr/bin/env bash

## Create default directories
mkdir ~/Downloads ~/School ~/Documents ~/Code ~/Media
mkdir ~/School/cs ~/Media/Wallpapers ~/Media/Screenshots ~/Media/Pictures ~/Media/Videos

# Default packages
sudo dnf install -y @base-x htop neofetch git wget kernel-devel \
    neovim feh fontawesome-fonts ranger alacritty \
    fish util-linux-user whois xautolock NetworkManager-wifi emacs stow

# Install Doom Emacs
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install

cd ~/.dotfiles
stow fish
stow emacs
cd ~

chsh -s $(which fish)
